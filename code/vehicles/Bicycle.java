// Patricia Bourjeily
package vehicles;

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    // Getters
    public String getManufacturer() {
        return manufacturer;
    }
    public int getNumberGears() {
        return numberGears;
    }
    public double getMaxSpeed() {
        return maxSpeed;
    }

    // Constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }   
    
    public String toString(){
        String text = "Manufacturer: " + this.manufacturer + " Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
        return text;
    }
}