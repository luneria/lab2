// Patricia Bourjeily
package mainStore;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[]args){

        // Create a Bicycle[] which will hold 4 Bicycle objects
        Bicycle[] myBikes = new Bicycle[4];
        myBikes[0] = new Bicycle("Specialized", 20, 40);
        myBikes[1] = new Bicycle("UnSpecialized", 10, 10);
        myBikes[2] = new Bicycle("Specialized", 40, 60);
        myBikes[3] = new Bicycle("UnSpecialized", 9, 30);

        // loop to cycle through the entire Bicycle[]
        for (int i = 0; i < myBikes.length; i++){
            System.out.println(myBikes[i]);
        }
    }
}
